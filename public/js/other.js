var myHelpers = (function() {

    return {
        togglePanelVisibility: function ($panel, $bgLayer, $body) {
            if ($panel.hasClass('open')) {
                $panel.removeClass('open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
                    $body.removeClass('hide-overflow');
                });
                $bgLayer.removeClass('is-visible');
            }
            else {
                $panel.addClass('open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
                    $body.addClass('hide-overflow');
                });
                $bgLayer.addClass('is-visible');
            }
        },

        toggleVideoLightBox: function($activeClass, $trigger, $container, $video, $close) {

            $($trigger).on('click', function(event) {
                event.preventDefault();
                $($container).addClass($activeClass);
                $($video).get(0).play();
            });

            $($container).on('click', function(event) {
                if ( $(event.target).is($close) || $(event.target).is($container) ) {
                    event.preventDefault();
                    $(this).removeClass($activeClass);
                    $($video).get(0).pause();
                }
            });

            $(document).keyup(function(event) {
                if (event.which == '27') {
                    $($container).removeClass($activeClass);
                    $($video).get(0).pause();
                }
            });
        }
    }
})();