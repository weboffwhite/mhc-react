var React        = require('react');
var ReactRouter  = require('react-router');
var Router       = ReactRouter.Router;
var Route        = ReactRouter.Route;
var IndexRoute   = ReactRouter.IndexRoute;
var Base         = require('./components/Base.jsx');
var PageHome     = require('./components/PageHome.jsx');
var PageProducts = require('./components/PageProducts.jsx');
var PageAbout    = require('./components/PageAbout.jsx');

var Routes = (
    <Router>
        <Route path="/" component={Base}>
            <IndexRoute component={PageHome}/>
            <Route path="/home" component={PageHome} />
            <Route path="/products" component={PageProducts} />
            <Route path="/about" component={PageAbout} />
        </Route>
    </Router>
);

module.exports = Routes;