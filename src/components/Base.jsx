var React   = require('react');
var MainNav = require('./global/MainNav.jsx');
var Header  = require('./global/Header.jsx');
var Footer  = require('./global/Footer.jsx');


var counter = 0;

/* Primary Links */
var primaryLinks = [
    {"id": counter++, "target": "#", "name": "Home"},
    {"id": counter++, "target": "#", "name": "About Us"},
    {"id": counter++, "target": "#", "name": "Products"},
    {"id": counter++, "target": "#", "name": "100% Grass-Fed"},
    {"id": counter++, "target": "#", "name": "News & Press"},
    {"id": counter++, "target": "#", "name": "Recipes"},
    {"id": counter++, "target": "#", "name": "Find Products"},
    {"id": counter++, "target": "#", "name": "Grass-Fed Gear"},
    {"id": counter++, "target": "#", "name": "Contact"}
];

/* Secondary Links */
counter = 0;
var secondaryLinks = [
    {"id": counter++, "target": "#", "name": "Terms of Use"},
    {"id": counter++, "target": "#", "name": "Privacy Policy"},
    {"id": counter++, "target": "#", "name": "For Retailers"}
];

/* Social Links */
counter = 0;
var socialLinks = [
    {"id": counter++, "target": "#", "name": "facebook"},
    {"id": counter++, "target": "#", "name": "twitter"},
    {"id": counter++, "target": "#", "name": "instagram"},
    {"id": counter++, "target": "#", "name": "pinterest"},
    {"id": counter++, "target": "#", "name": "share-square-o"}
];



var Base = React.createClass({

    componentDidMount: function() {
        var $header         = $('#mhcHeader');
        var $headerEl       = $('.mhc-header-element');
        var $logo           = $('.mhc-header-logo-img');
        var $mainNavTrigger = $('.mhc-main-nav-trigger');
        var $mainNavIcon    = $('.mhc-main-nav-icon');
        var $mainNav        = $('.mhc-main-nav');
        var $page           = $('.mhc-page');
        var $body           = $('body');

        var heroBgHeight    = $('#mhcHeroBg').height();


        // HEADER STATUS HANDLER
        // =====================

        var makeHeaderActive = function() {
            $headerEl.addClass('active', 300);
            $logo.attr('src', 'img/maplehill-grass-fed-logomark.png');
        };

        var makeHeaderPassive = function() {
            $headerEl.removeClass('active', 300);
            $logo.attr('src', 'img/maplehill-grass-fed-logomark-white.png');
        };
        
        var headerSlide = function() {
            $(window).on('scroll', function() {    
                var scroll = $(window).scrollTop();
                (scroll >= heroBgHeight - 100) ? makeHeaderActive() : makeHeaderPassive();
            });
        }();


        // NAVIGATION HANDLER
        // ==================

        var triggerNavigation = function() {
            $mainNavTrigger.on('click', function(event) {
                event.preventDefault();
                var scroll = $(window).scrollTop();

                if ( $mainNav.hasClass('open') ) {

                    $mainNav.removeClass('open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                        $body.removeClass('overflow-hidden');
                    });

                    $mainNavIcon.removeClass('clicked');
                    $header.removeClass('open-nav');
                    $page.removeClass('scaled-down');

                    if (scroll >= heroBgHeight) {
                        makeHeaderActive();
                    }
                } 
                else {
                    $mainNav.addClass('open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                        $body.addClass('overflow-hidden');
                    }); 

                    $mainNavIcon.addClass('clicked');
                    makeHeaderPassive();
                    $header.addClass('open-nav');
                    $page.addClass('scaled-down');
                }
            });
        }();
    },
    render: function() {
        return (
            <div className="mhc-base">
                <MainNav primaryLinks={primaryLinks} secondaryLinks={secondaryLinks} socialLinks={socialLinks} />

                <Header />
                
                <div className="mhc-page">
                    {this.props.children}
                    <Footer primaryLinks={primaryLinks} secondaryLinks={secondaryLinks} socialLinks={socialLinks} />
                </div>
            </div>
        );
    }
});

module.exports = Base;