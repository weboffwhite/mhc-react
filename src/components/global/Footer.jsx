var React  = require('react');
var NavLinks = require('./NavLinks.jsx');

var Footer = React.createClass({
    componentDidMount: function() {
        // Back to Top
        $("#mhcBackToTop").on("click", function(event) {

            event.preventDefault();

            $('body, html').animate({ scrollTop: 0 }, 800, function() {
                // window.location.hash = "#";
            });
        });
    },
    render: function() {        
        return(
            <footer className="mhc-main-footer clearfix">
                <div className="mhc-footer-logo">
                    <img src="img/maplehill-grass-fed-logomark-white.png" />
                </div>

                <nav className="mhc-main-nav-body">
                    <NavLinks linksListClass={"mhc-main-nav-list"} linksListType={"primary"} linksList={this.props.primaryLinks} />
                </nav>
                
                <div className="mhc-main-nav-footer">
                    <NavLinks linksListClass={"mhc-main-nav-social-list mhc-links-list"} linksListType={"social"} linksList={this.props.socialLinks} />
                    
                    <p>@2015 Maple Hill Creamery, LLC. All rights reserved.</p>
                    
                    <NavLinks linksListClass={"mhc-main-subnav-list mhc-links-list"} linksListType={"secondary"} linksList={this.props.secondaryLinks} />

                    <div className="mhc-back-to-top">
                        <a href="#" id="mhcBackToTop" className="clearfix mhc-back-to-top-link">
                            <span className="mhc-inner">
                                <span className="mhc-icon"><i className="fa fa-angle-up"></i></span>
                                <span className="mhc-copy">Back to top</span>
                            </span>
                        </a>
                    </div>
                </div>
            </footer>
        );
    }
});

module.exports = Footer;