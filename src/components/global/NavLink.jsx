var React = require('react');

var NavLink = React.createClass({
    render: function() {
        
        var linkType = this.props.linkType;

        switch (linkType) {
            case "primary":
                return (<li><a href={this.props.linkTarget}><span>{this.props.linkName}</span></a></li>);
                break;
            case "secondary":
                return (<li><a href={this.props.linkTarget}>{this.props.linkName}</a></li>);
                break;
            case "social":
                return (<li><a href={this.props.linkTarget}><i className={"fa fa-" + this.props.linkName}></i></a></li>);
                break;
            case "header":
                return (
                    <li>
                        <a href={this.props.linkTarget}>
                            <span className="mhc-link clearfix">
                                <span className={"mhc-link-icon mhc-link-element mhc-header-element mhc-" + this.props.linkName.toLowerCase()}></span>
                                <span className="mhc-link-copy mhc-link-element">
                                    <span className="mhc-header-element">{this.props.linkName}</span>
                                </span>
                            </span>
                        </a>
                    </li>  
                );
                break;
            default:
                return (<li><a href={this.props.linkTarget}>{this.props.linkName}</a></li>);
        }
    }
});

module.exports = NavLink;