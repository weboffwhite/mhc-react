var React = require('react');
var HeroCallout = require('./HeroCallout.jsx');



var HeroCallouts = React.createClass({
    render: function() {
        var callouts    = this.props.callouts;
        var calloutsQty = callouts.length;
        var listOfCallouts;

        if (callouts) {
            listOfCallouts = callouts.map(function(callout) {
                return (<HeroCallout key={callout.id} title={callout.title} subtitle={callout.subtitle} img={callout.img} link={callout.link} items={calloutsQty} />);
            });

            return (
                <div className="mhc-hero-callouts clearfix">
                    <ul>{listOfCallouts}</ul>
                </div>
            );
        }
        else {
            return (false);
        }
    }
});

module.exports = HeroCallouts;