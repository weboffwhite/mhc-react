var React = require('react');

var HeroCallout = React.createClass({
    render: function() {
        var calloutBg = {
            backgroundImage: "url(" + this.props.img + ")"
        }

        var columnsQty = (this.props.items % 2 === 0) ? "tablet-6 desktop-3" : "tablet-4";
        
        return (
            <li className={"phone-12 " + columnsQty + " columns mhc-hero-callout mhc-brick"} style={calloutBg}>
                <a href={this.props.link}>
                    <div className="mhc-shadow"></div>
                    <div className="mhc-hero-callout-copy">
                        <h4 className="mhc-title">{this.props.title}</h4>
                        <h5 className="mhc-subtitle">{this.props.subtitle}</h5>
                    </div>
                </a>
            </li>
        );
    }
});

module.exports = HeroCallout;