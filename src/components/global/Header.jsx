var React = require('react');
var NavLinks = require('./NavLinks.jsx');

var counter = 0;
var headerLinks = [
    { "id": counter++, "target": "#", "name": "Stores" },
    { "id": counter++, "target": "#", "name": "Farms"  }
];

var Header = React.createClass({
    
    render: function() {

        return (
            <header id="mhcHeader" className="mhc-header mhc-header-element">
                <div className="row">
                    <a className="mhc-main-nav-trigger mhc-header-element" href="#0">
                        <span className="mhc-main-nav-text mhc-header-element">Menu</span>
                        <span className="mhc-main-nav-icon mhc-header-element"></span>
                    </a>

                    <div className="mhc-header-logo">
                        <img src="img/maplehill-grass-fed-logomark-white.png" className="mhc-header-logo-img"/>
                    </div>

                    <div className="mhc-header-links">
                        <NavLinks linksListClass={"mhc-header-list"} linksListType={"header"} linksList={headerLinks} />
                    </div>
                </div>
            </header>
        );
    }
});

module.exports = Header;
