var React = require('react');
var NavLinks = require('./NavLinks.jsx');

var MainNav = React.createClass({
    render: function() {
        return(
            <div className="mhc-main-nav">
                <nav className="mhc-main-nav-body">
                    <NavLinks linksListClass={"mhc-main-nav-list"} linksListType={"primary"} linksList={this.props.primaryLinks} />
                </nav>
                
                <div className="mhc-main-nav-footer">
                    <NavLinks linksListClass={"mhc-main-nav-social-list mhc-links-list"} linksListType={"social"} linksList={this.props.socialLinks} />
                    
                    <p>@2015 Maple Hill Creamery, LLC. All rights reserved.</p>
                    
                    <NavLinks linksListClass={"mhc-main-subnav-list mhc-links-list"} linksListType={"secondary"} linksList={this.props.secondaryLinks} />
                </div>
            </div>
        );
    }
});

module.exports = MainNav;