var React = require('react');
var HeroCallouts = require('./hero/HeroCallouts.jsx');

var Hero = React.createClass({

    render: function() {
        
        var heroBgImg = {
            backgroundImage: "url(" + this.props.img + ")"
        }

        return (
            <section id="mhcHero" className="mhc-hero">
                <div id="mhcHeroBg" className="mhc-hero-bg" style={heroBgImg}>
                    <div className="mhc-copy">
                        <div className="row">
                            <div className="mhc-copy-container">
                                <h5 className="mhc-pretitle">{this.props.pretitle}</h5>
                                <h1 className="mhc-title" dangerouslySetInnerHTML={{__html: this.props.title}} />
                                <p className="mhc-description">{this.props.description}</p>
                                <div className="mhc-call-to-action-btns">
                                    <a href="#" className="mhc-btn highlighted mhc-modal-trigger-video">
                                        <span className="mhc-btn-copy">
                                            Watch Video <span className="play"><i className="fa fa-play"></i></span>
                                        </span>
                                    </a>
                                    <a href={this.props.callToActionLink} className="mhc-btn">
                                        <span className="mhc-btn-copy">
                                            {this.props.callToAction}
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <HeroCallouts callouts={this.props.callouts} />
            </section>
        );
    }
});

module.exports = Hero;
