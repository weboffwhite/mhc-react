var React = require('react');
var NavLink = require('./NavLink.jsx');

var NavLinks = React.createClass({
    render: function() {
        var linksListClass = this.props.linksListClass;
        var linksListType  = this.props.linksListType;
        var linksList      = this.props.linksList;

        var linkItems = linksList.map(function(link) {
            return (
                <NavLink key={link.id} linkTarget={link.target} linkName={link.name} linkType={linksListType} />
            );
        });

        return (<ul className={linksListClass}>{linkItems}</ul>);
    }
});

module.exports = NavLinks;