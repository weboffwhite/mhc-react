var React = require('react');

var HomeFeature = React.createClass({
    render: function() {
        return(
            <section className="mhc-home-feature mhc-double-border clearfix">
                <div className="inner-outer">
                    <div className="inner-inner">
                        <div className="row">
                            <div className="mhc-copy-container">
                                <h5 className="mhc-pretitle">Certified 100% Grass-Fed</h5>
                                <h3 className="mhc-title">No Corn, No Grain<br />Just Grass</h3>
                                <img src="img/maplehill-grass-fed-products-gloryshot.png" className="mhc-img" />
                                <p className="mhc-description">Our Organic, artisanal-quality products have a uniquely fresh, earthy flavor and buttery-smooth texture that comes from using only 100% grass-fed full-fat organic milk. We don't add excess sugar, artificial flavors, unnecessary thickeners, or added color to our products. Since day one, we've kept our recipes traditional and uncomplicated, staying true to our philosophy that the best foods are simply made with exceptional ingredients.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
});

module.exports = HomeFeature;
