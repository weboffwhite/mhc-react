var React = require('react');
var HomeMoreInfo = React.createClass({
    render: function() {
        return(
            <section className="mhc-home-more-info clearfix">

                <div className="phone-12 laptop-6 columns padding-0 mhc-subsections-container">
                    <a href="#" className="phone-12 columns padding-0 mhc-subsection mhc-brick mhc-recipe-book">
                        <div className="mhc-shadow"></div>
                        <div className="mhc-inner">
                            <img src="img/maplehill-grass-fed-cow-accent-white.png" className="mhc-icon" />
                            <h5 className="mhc-pretitle">100% Grass-Fed</h5>
                            <h2 className="mhc-title">Recipe Book</h2>
                        </div>
                    </a>
                </div>

                <div className="phone-12 laptop-6 columns padding-0 mhc-subsections-container">
                    <a href="#" className="phone-12 columns padding-0 mhc-subsection mhc-brick mhc-news-press">
                        <div className="mhc-shadow"></div>
                        <div className="mhc-inner">
                            <img src="img/maplehill-grass-fed-cow-accent-white.png" className="mhc-icon" />
                            <h5 className="mhc-pretitle">Maple Hill Blog</h5>
                            <h2 className="mhc-title">News & Press</h2>
                        </div>
                    </a>

                    <a href="#" className="phone-12 columns padding-0 mhc-subsection mhc-brick mhc-milkshed">
                        <div className="mhc-shadow"></div>
                        <div className="mhc-inner">
                            <img src="img/maplehill-grass-fed-cow-accent-white.png" className="mhc-icon" />
                            <h5 className="mhc-pretitle">Support Grass Farmers</h5>
                            <h2 className="mhc-title">Milkshed</h2>
                        </div>
                    </a>
                </div>

            </section>
        );
    }
});

module.exports = HomeMoreInfo;
