var React = require('react');
var HomeVideoModal = React.createClass({
    render: function() {
        return (
            <div className="mhc-modal mhc-video-modal" role="alert">
                <div className="mhc-modal-inner">
                    <video controls className="mhc-video" id="mhcVideo">
                        <source src="video/mhc-video.mp4" type="video/mp4"></source>
                        Your browser does not support the video tag.
                    </video>
                    <a href="#0" className="mhc-modal-close img-replace">Close</a>
                </div>
            </div>
        );
    }
});

module.exports = HomeVideoModal;