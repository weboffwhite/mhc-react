var React = require('react');

var HomeNewsletter = React.createClass({
    render: function() {
        return(
            <section className="mhc-home-newsletter mhc-home-feature clearfix">
                <div className="inner-outer">
                    <div className="inner-inner">
                        <div className="row">
                            <div className="mhc-copy-container">
                                <h5 className="mhc-pretitle">Join the herd!</h5>
                                <h3 className="mhc-title">Newsletter</h3>
                                <div className="mhc-form-container">
                                    <form>
                                        <input type="text" className="mhc-text-input" placeholder="Email Address..." />
                                        <input type="submit" value=">" className="mhc-submit" />
                                    </form>
                                </div>
                                <p className="mhc-description">Want to have access to exclusive coupons, recipes, fun contests and the occasional cow joke?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        );
    }
});

module.exports = HomeNewsletter;