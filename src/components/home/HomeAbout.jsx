var React = require('react');
var HomeAbout = React.createClass({
    render: function() {
        return(
            <section className="mhc-home-about clearfix">
                <div className="phone-12 laptop-6 columns padding-0 mhc-subsections-container">
                    <a href="#" className="phone-12 columns padding-0 mhc-subsection mhc-brick mhc-our-history">
                        <div className="mhc-shadow"></div>
                        <div className="mhc-inner">
                            <img src="img/maplehill-grass-fed-cow-accent-white.png" className="mhc-icon" />
                            <h5 className="mhc-pretitle">About Maple Hill</h5>
                            <h2 className="mhc-title">Our History</h2>
                        </div>
                    </a>

                    <a href="#" className="phone-12 columns padding-0 mhc-subsection mhc-brick mhc-grass-facts">
                        <div className="mhc-shadow"></div>
                        <div className="mhc-inner">
                            <img src="img/maplehill-grass-fed-cow-accent-white.png" className="mhc-icon" />
                            <h5 className="mhc-pretitle">It starts with one blade of grass</h5>
                            <h2 className="mhc-title">Grass Facts</h2>
                        </div>
                    </a>
                </div>

                <div className="phone-12 laptop-6 columns padding-0 mhc-subsections-container">
                    <a href="#" className="phone-12 columns padding-0 mhc-subsection mhc-brick mhc-our-mission">
                        <div className="mhc-shadow"></div>
                        <div className="mhc-inner">
                            <img src="img/maplehill-grass-fed-cow-accent-white.png" className="mhc-icon" />
                            <h5 className="mhc-pretitle">100% Grass-Fed</h5>
                            <h2 className="mhc-title">Our Mission</h2>
                        </div>
                    </a>
                </div>
            </section>
        );
    }
});

module.exports = HomeAbout;
