var React = require('react');
var Hero  = require('./global/Hero.jsx');
var ProductsCategory = require('./products/ProductsCategory.jsx');

/* CREAM ON TOP */
var counter = 0;
var productsCreamOnTop = [
    { "id" : counter++, "title": "Plain",          "img": "img/product-creamontop-6-large-plain.png",     "description": "Zingy, tart, and authentic, the yougurt eater's yogurt.",        "className": "plain"     },
    { "id" : counter++, "title": "Lemon",          "img": "img/product-creamontop-6-large-lemon.png",     "description": "Just a tad of real lemon extract. Fresh, citrusy, and bright.",  "className": "lemon"     },
    { "id" : counter++, "title": "Vanilla",        "img": "img/product-creamontop-6-large-vanilla.png",   "description": "Real vanilla bean extract is a no-brainer for superior flavor.", "className": "vanilla"   },
    { "id" : counter++, "title": "Maple",          "img": "img/product-creamontop-6-large-maple.png",     "description": "Only 100% organic maple syrup sweetens this classic flavor.",    "className": "maple"     },
    { "id" : counter++, "title": "Orange Creme",   "img": "img/product-creamontop-6-large-orange.png",    "description": "Think orange-creme soda, without all the sugar.",                "className": "orange"    },
    { "id" : counter++, "title": "Wild Blueberry", "img": "img/product-creamontop-6-large-blueberry.png", "description": "Wildly delicious puree for an intense blueberry experience.",    "className": "blueberry" },
    { "id" : counter++, "title": "Plain",          "img": "img/product-creamontop-32-large-plain2.png",   "description": "Zingy, tart, and authentic, the yougurt eater's yogurt.",        "className": "plain"     },
    { "id" : counter++, "title": "Maple",          "img": "img/product-creamontop-32-large-maple2.png",   "description": "Only 100% organic maple syrup sweetens this classic flavor.",    "className": "maple"     },
    { "id" : counter++, "title": "Vanilla",        "img": "img/product-creamontop-32-large-vanilla2.png", "description": "Real vanilla bean extract is a no-brainer for superior flavor.", "className": "vanilla"   }
];

/* DRINKABLE */
counter = 0;
var productsDrinkable = [
    { "id": counter++, "title": "Plain",          "img": "img/product-drinkable-large-plain.png",     "description": "Zingy, tart, and authentic, the yougurt eater's yogurt.",        "className": "plain"     },
    { "id": counter++, "title": "Maple",          "img": "img/product-drinkable-large-maple.png",     "description": "Only 100% organic maple syrup sweetens this classic flavor.",    "className": "maple"     },
    { "id": counter++, "title": "Vanilla",        "img": "img/product-drinkable-large-vanilla.png",   "description": "Real vanilla bean extract is a no-brainer for superior flavor.", "className": "vanilla"   },
    { "id": counter++, "title": "Lemon",          "img": "img/product-drinkable-large-lemon.png",     "description": "Just a tad of real lemon extract. Fresh, citrusy, and bright.",  "className": "lemon"     },
    { "id": counter++, "title": "Orange Creme",   "img": "img/product-drinkable-large-orange.png",    "description": "Think orange-creme soda, without all the sugar.",                "className": "orange"    },
    { "id": counter++, "title": "Wild Blueberry", "img": "img/product-drinkable-large-blueberry.png", "description": "Wildly delicious puree for an intense blueberry experience.",    "className": "blueberry" }
];

/* KEFIR */
counter = 0;
var productsKefir = [
    { "id": counter++, "title": "Plain",      "img": "img/product-kefir-large-plain.png",      "description": "Superbly smooth, and no added anything (except lots of love).",    "className": "plain"      },
    { "id": counter++, "title": "Strawberry", "img": "img/product-kefir-large-strawberry.png", "description": "The best berries, not 'natural flavor' for a subtly sweet treat.", "className": "strawberry" },
    { "id": counter++, "title": "Vanilla",    "img": "img/product-kefir-large-vanilla.png",    "description": "Real vanilla extract, lightly sweetened with organic cane sugar.", "className": "vanilla"    }
];

/* GREEK */
counter = 0;
var productsGreek = [
    { "id": counter++, "title": "Plain",        "img": "img/product-greek-53-large-plain.png",      "description": "Just our milk & cultures: ultra-creamy perfection.",            "className": "plain"      },
    { "id": counter++, "title": "Raspberry",    "img": "img/product-greek-53-large-raspberry.png",  "description": "Real raspberries rock, added 'natural' flavor need not apply.", "className": "raspberry"  },
    { "id": counter++, "title": "Strawberry",   "img": "img/product-greek-53-large-strawberry.png", "description": "Organic strawberries, subtly sweet & fresh.",                   "className": "strawberry" },
    { "id": counter++, "title": "Vanilla Bean", "img": "img/product-greek-53-large-vanilla.png",    "description": "Satisfyingly speckled with deepest vanilla flavor.",            "className": "vanilla"    },
    { "id": counter++, "title": "Peach",        "img": "img/product-greek-53-large-peach.png",      "description": "Organic yellow peaches, sweet-tart & summery.",                 "className": "peach"      },
    { "id": counter++, "title": "Plain",        "img": "img/product-greek-16-large-plain.png",      "description": "Just our milk & cultures: ultra-creamy perfection.",            "className": "plain"      },
    { "id": counter++, "title": "Vanilla Bean", "img": "img/product-greek-16-large-vanilla.png",    "description": "Satisfyingly speckled with deepest vanilla flavor.",            "className": "vanilla"    }
];

/* RAW MILK CHEESE */
var productsRawMilkCheese = [
    { "id": counter++, "title": "Stone Creek Cheddar", "img": "img/product-cheese-7-large-stonecreek.png",  "description": "A fresh cheddar, with sweet cream and floral notes.",       "className": "stonecreek" },
    { "id": counter++, "title": "One Year Cheddar",    "img": "img/product-cheese-7-large-cheddar.png",     "description": "Flavorfully piquiant, redolent of fresh green grass.", "className": "cheddar"    },
    { "id": counter++, "title": "Darma Lea Dutch",     "img": "img/product-cheese-7-large-dutch.png",       "description": "Rich and creamy gouda-style butterscotch undertones.", "className": "dutch"      },
    { "id": counter++, "title": "Stone Creek Cheddar", "img": "img/product-cheese-16-large-stonecreek.png", "description": "A fresh cheddar, with sweet cream and floral notes.",       "className": "stonecreek" },
    { "id": counter++, "title": "One Year Cheddar",    "img": "img/product-cheese-16-large-cheddar.png",    "description": "Flavorfully piquiant, redolent of fresh green grass.", "className": "cheddar"    },
    { "id": counter++, "title": "Darma Lea Dutch",     "img": "img/product-cheese-16-large-dutch.png",      "description": "Rich and creamy gouda-style butterscotch undertones.", "className": "dutch"      }
];

var productsHeroDescription = "Our organic, artisanal-quality products have a uniquely fresh, earthy flavor and buttery-smooth texture that comes from using only 100% grass-fed full-fat organic milk.";


var PageProducts =  React.createClass({
    componentDidMount: function() {
        
        var accordionHandler = function() {

            $('.mhc-products-accordion-link').on('click', function(event) {
                event.preventDefault();

                var $parent = $(this).parent();
                var $panel = $parent.next();

                if ($parent.hasClass('opened')) {
                    $(this).find('.mhc-link-icon').text('+');
                    $panel.slideUp();
                    $parent.removeClass('opened');
                }
                else {
                    $(this).find('.mhc-link-icon').text('-');
                    $panel.slideDown();
                    $parent.addClass('opened');
                }
    
                return false;
            });  
        }();

        var categoryHandler = function() {
            var $productsContainer = $('#mhcProductsContainer').isotope({
                itemSelector: '.mhc-products-category'
            });

            var $productsCategoryOutput = $('#mhcProductCategoryOutput');
            var $productsCheckboxes     = $('#mhcProductsCheckboxes input');

            $productsCheckboxes.on('change', function() {
                var inclusives = [];
                var names      = [];

                var currentLists = $('.mhc-products-category').find('li').length;

                if (currentLists === 0) {
                    $('.mhc-products-category').hide();
                }

                $productsCheckboxes.each(function(i, elem) {
                    if (elem.checked) {
                        inclusives.push(elem.value);
                        names.push(elem.name);
                    }
                });

                var filterValue = inclusives.length ? inclusives.join(', ') : '*';
                var filterName  = names.length      ?      names.join(', ') : '*';

                $productsCategoryOutput.text(filterName);
                $productsContainer.isotope({
                    filter: filterValue
                });
            });            
        }();

        var flavorHandler = function() {
            
            var currentClass, index, joinedFilters;
            var $product = $('.mhc-product');
            var filters = [];


            $('.mhc-flavor-btn').on('click', function() {

                $('body, html').animate({ scrollTop: $('#mhcHero').height() - 100}, 800, function() {
                    // window.location.hash = "#";
                });

                var $target = $(event.currentTarget);

                if ($target.hasClass('checked')) {
                    currentClass = $target.data('filter');
                    $product.filter(currentClass).fadeOut();
                    $target.removeClass('checked');

                    index = filters.indexOf(currentClass);
                    filters.splice(index, 1);
                    joinedFilters = filters.join(', ');
                    
                    (filters.length == 0) ? $product.fadeIn('slow') : $product.filter(joinedFilters).fadeIn('slow');
                }
                else {
                    $target.addClass('checked');
                    currentClass = $target.data('filter');
                    filters.push(currentClass);
                    joinedFilters = filters.join(', ');
                    $product.fadeOut('slow');
                    $product.filter(joinedFilters).fadeIn('slow');
                }

            });
        }();

        var lockProductFilters = function() {

            var $productFilters = $('.mhc-products-option-selectors-container');
            var fixingPoint     = $('.mhc-products-wrapper').offset().top;
            var releasePoint    = $('.mhc-main-footer').offset().top;
            var headerHeight    = $('#mhcHeader').height();

            var fixProductFilters = function() {
                $productFilters.addClass('fixed');
            };

            var unfixProductFilters = function() {
                $productFilters.removeClass('fixed');
            };

            $(window).on('scroll', function() {
                var scroll = $(window).scrollTop();
                (scroll >= fixingPoint - 100) ? fixProductFilters() : unfixProductFilters();
            });
        }();
    },
    render: function() {

        /* CREAM ON TOP */
        var creamOnTopClass          = "cream-on-top";
        var creamOnTopTitle          = "Cream on Top Yogurt";
        var creamOnTopDescription    = "<p>Our hallmark product, created on our family farm's stove top in 2008. Tastefully tart, incredibly smooth, topped with a luxurious 'creamline' layer.</p>";

        /* DRINKABLE */
        var drinkableClass           = "drinkable";
        var drinkableTitle           = "Drinkable Yogurt";
        var drinkableDescription     = "<p>Pourable, portable, and shareable - a deliciously creamy 100% grass-fed meal-in-a-bottle.</p>";

        /* KEFIR */
        var kefirClass               = "kefir";
        var kefirTitle               = "'Cleaner' Kefir";
        var kefirDescription         = "<p>Packed with belly-friendly probiotics, our 'cleaner' kefir is super creamy, mildly tart and refreshing.</p>";

        /* GREEK */
        var greekClass               = "greek";
        var greekTitle               = "Greek Yogurt";
        var greekDescription         = "<p>Authentically strained, our mildy tart Greek yogurt is silky-smooth, palate-friendly, and packed with 11-12 grams of protein per serving.</p>";

        /* RAW MILK CHEESE */
        var rawMilkCheeseClass       = "raw-milk-cheese";
        var rawMilkCheeseTitle       = "Raw Milk Cheese";
        var rawMilkCheeseDescription = "<p>Handcrafted in Vermont by the artisans at <a href='http://www.graftonvillagecheese.com/'>Grafton Village</a>, our aged premium cheeses are made exclusively with our 100% grass-fed raw milk.</p>"
        
        return (

            <div id="mhcProducts" className="mhc-page-body mhc-products">

                <Hero callouts={false} 
                      img={"img/mhc-products-bg.jpg"} 
                      pretitle={"Certified 100% Grass Fed"} 
                      title={"Products"} 
                      description={productsHeroDescription} 
                      callToAction={"About"}
                      callToActionLink={"#/about"} />
                
                <section className="mhc-products-wrapper clearfix">
                    
                    <aside className="mhc-products-option-selectors-container mhc-double-border">

                        <div className="inner-outer">

                            <div className="inner-inner">

                                <div className="mhc-products-accordion">

                                    <div className="mhc-products-accordion-title opened">
                                        <a className="mhc-products-accordion-link" href="#">
                                            <span className="mhc-link-copy">Filter by Category</span>
                                            <span className="mhc-link-icon">-</span>
                                        </a>
                                    </div>
                                    
                                    <div id="mhcProductsCheckboxes" className="mhc-products-accordion-body mhc-category-selector-container">
                                        <p><label><input type="checkbox" value=".cream-on-top" name="Cream on Top Yogurt" /> <span>Cream on Top Yogurt</span></label></p>
                                        <p><label><input type="checkbox" value=".drinkable" name="Drinkable Yogurt" /> <span>Drinkable Yogurt</span></label></p>
                                        <p><label><input type="checkbox" value=".greek" name="Greek Yogurt" /> <span>Greek Yogurt</span></label></p>
                                        <p><label><input type="checkbox" value=".kefir" name="Cleaner Kefir" /> <span>"Cleaner" Kefir</span></label></p>
                                        <p><label><input type="checkbox" value=".raw-milk-cheese" name="Raw Milk Cheese" /> <span>Raw Milk Cheese</span></label></p>
                                    </div>

                                    <div className="mhc-products-accordion-title opened flavors">
                                        <a className="mhc-products-accordion-link" href="#">
                                            <span className="mhc-link-copy">Filter by Flavor</span>
                                            <span className="mhc-link-icon">-</span>
                                        </a>
                                    </div>
                                    <div className="mhc-products-accordion-body mhc-flavors-selector-container">
                                        <div className="row">
                                            <button className="phone-6 columns mhc-flavor-btn" data-filter=".plain">
                                                <img src="img/mhc-flavor-filter-plain.png" className="mhc-flavor-img" />
                                                <h6 className="mhc-btn-copy">Plain</h6>
                                            </button>
                                            <button className="phone-6 columns mhc-flavor-btn" data-filter=".maple">
                                                <img src="img/mhc-flavor-filter-maple2.png" className="mhc-flavor-img" />
                                                <h6 className="mhc-btn-copy">Maple</h6>
                                            </button>
                                            <button className="phone-6 columns mhc-flavor-btn" data-filter=".vanilla">
                                                <img src="img/mhc-flavor-filter-vanilla.png" className="mhc-flavor-img" />
                                                <h6 className="mhc-btn-copy">Vanilla</h6>
                                            </button>
                                            <button className="phone-6 columns mhc-flavor-btn" data-filter=".lemon">
                                                <img src="img/mhc-flavor-filter-lemon.png" className="mhc-flavor-img" />
                                                <h6 className="mhc-btn-copy">Lemon</h6>
                                            </button>
                                            <button className="phone-6 columns mhc-flavor-btn" data-filter=".blueberry">
                                                <img src="img/mhc-flavor-filter-blueberry.png" className="mhc-flavor-img" />
                                                <h6 className="mhc-btn-copy">Blueberry</h6>
                                            </button>
                                            <button className="phone-6 columns mhc-flavor-btn" data-filter=".orange">
                                                <img src="img/mhc-flavor-filter-orange.png" className="mhc-flavor-img" />
                                                <h6 className="mhc-btn-copy">Orange Creme</h6>
                                            </button>
                                            <button className="phone-6 columns mhc-flavor-btn" data-filter=".strawberry">
                                                <img src="img/mhc-flavor-filter-strawberry.png" className="mhc-flavor-img" />
                                                <h6 className="mhc-btn-copy">Strawberry</h6>
                                            </button>
                                            <button className="phone-6 columns mhc-flavor-btn" data-filter=".peach">
                                                <img src="img/mhc-flavor-filter-peach.png" className="mhc-flavor-img" />
                                                <h6 className="mhc-btn-copy">Peach</h6>
                                            </button>
                                            <button className="phone-6 columns mhc-flavor-btn" data-filter=".raspberry">
                                                <img src="img/mhc-flavor-filter-raspberry.png" className="mhc-flavor-img" />
                                                <h6 className="mhc-btn-copy">Raspberry</h6>
                                            </button>
                                            
                                        </div>
                                    </div>
                                    {/*
                                    <div className="mhc-products-accordion-title opened">
                                        <a className="mhc-products-accordion-link" href="#">
                                            <span className="mhc-link-copy">Filter by Size</span>
                                            <span className="mhc-link-icon">+</span>
                                        </a>
                                    </div>
                                    <div className="mhc-products-accordion-body">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Nullam mollis. Ut justo. Suspendisse potenti.</div>
                                    */}
                                </div>

                            </div>

                        </div>

                    </aside>

                    <div className="mhc-products-container-all clearfix">

                        {/*
                        <div className="mhc-products-container-all-header">
                            <p id="mhcProductCategoryOutput"></p>
                        </div>
                        */}
                        
                        <div id="mhcProductsContainer" className="mhc-products-container-by-category clearfix">
                            
                            <ProductsCategory productsList={productsCreamOnTop}    className={creamOnTopClass}    title={creamOnTopTitle}    description={creamOnTopDescription}    />
                            <ProductsCategory productsList={productsDrinkable}     className={drinkableClass}     title={drinkableTitle}     description={drinkableDescription}     />
                            <ProductsCategory productsList={productsKefir}         className={kefirClass}         title={kefirTitle}         description={kefirDescription}         />
                            <ProductsCategory productsList={productsGreek}         className={greekClass}         title={greekTitle}         description={greekDescription}         />
                            <ProductsCategory productsList={productsRawMilkCheese} className={rawMilkCheeseClass} title={rawMilkCheeseTitle} description={rawMilkCheeseDescription} />

                        </div>
                        
                    </div>
                    
                </section>

            </div>
        );
    }
});

module.exports = PageProducts;