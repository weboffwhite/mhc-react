var React = require('react');

var counter = 0;
var products = {
    "yogurtPlain": { 
        "id": ++counter, 
        "slug": "yogurt-plain",
        "title": "Plain", 
        "category": "Cream On Top",
        "categorySlug": "cream-on-top",
        "img": "img/product-greek-53-large-plain.png", 
        "description": "Just our milk & cultures: ultra-creamy perfection.",  
        "ingredients": {
            "slug": "ingredients",
            "title": "Ingredients",
            "icon": "img/mhc-product-details-ingredients.png",
            "list": "Pasteurized whole organic milk, organic sugar, live yogurt cultures. (b. lactis, L. acidophilus, L. Delbruecki subsp, bulgaricus, S. thermophilus)."
        },
         
        "nutritionFacts": {
            "slug": "nutrition-facts",
            "title": "Nutrition Facts",
            "icon": "img/mhc-product-details-nutritionfacts.png",
            "dv": "%DV*",

            "calories": {
                "slug": "calories",
                "title": "Calories",
                "count": "150",
                "dailyValue": ""
            },
            "caloriesFromFat": {
                "slug": "calories-from-fat",
                "title": "Calories from Fat",
                "count": "70",
                "dailyValue": ""
            },
            "totalFat": {
                "slug": "total-fat",
                "title": "Total fat",
                "count": "7g",
                "dailyValue": "11%",
            },
            "saturatedFat": {
                "slug": "saturated-fat",
                "title": "Saturated Fat",
                "count": "4.5g",
                "dailyValue": "24%"
            },
            "transFat": {
                "slug": "trans-fat",
                "title": "Trans Fat",
                "count": "0g",
                "dailyValue": ""
            },
            "cholesterol": {
                "slug": "cholesterol",
                "title": "Cholesterol",
                "count": "30mg",
                "dailyValue": "10%"
            },
            "sodium": {
                "slug": "sodium",
                "title": "Sodium",
                "count": "80mg",
                "dailyValue": "3%"
            },
            "carbs": {
                "slug": "carbs",
                "title": "Total Carbohidrates",
                "count": "14g",
                "dailyValue": "5%",
            },
            "dailyFiber": {
                "slug": "daily-fiber",
                "title": "Daily Fiber",
                "count": "0g",
                "dailyValue": "0%"
            },
            "sugar": {
                "slug": "sugar",
                "title": "Sugar",
                "count": "14g",
                "dailyValue": ""
            },
            "protein": {
                "slug": "protein",
                "title": "Protein",
                "count": "6g",
                "dailyValue": ""
            },
            "vitaminA": {
                "slug": "vitamin-a",
                "title": "Vitamin A",
                "count": "",
                "dailyValue": "4%"
            },
            "vitaminC": {
                "slug": "vitamin-c",
                "title": "Vitamin C",
                "count": "",
                "dailyValue": "4%"
            },
            "calcium": {
                "slug": "calcium",
                "title": "Calcium",
                "count": "",
                "dailyValue": "20%"
            },
            "iron": {
                "slug": "iron",
                "title": "Iron",
                "count": "",
                "dailyValue": "0%"
            }
        },
        "certifications": {
            "slug": "certifications",
            "title": "Certifications",
            "icon": "img/mhc-product-details-certifications.png",

            "usdaOrganic": {
                "slug": "usda-organic",
                "title": "USDA Organic",
                "status": true
            },

            "kosher": {
                "slug": "kosher",
                "title": "Kosher",
                "status": true
            },
            "grassFed": {
                "slug": "grass-fed",
                "title": "PCO Certified 100% Grass-Fed",
                "status": true
            },
            "glutenFree": {
                "slug": "gluten-free",
                "title": "Gluten Free",
                "status": true
            },
            "gradeA": {
                "slug": "grade-a",
                "title": "Grade A",
                "status": true
            }
        }
    }
}

var product = products.yogurtPlain;
var facts   = product.nutritionFacts;
var ingredients = product.ingredients;
var certifications = product.certifications;

var getValue = function(value) {
    if (value === "") {
        return "-";
    }
    else if (value === true) {
        return "Yes";
    }
    else if (value === false) {
        return "No";
    }
    else {
        return value;
    }
}


var ProductDetail = React.createClass({
    render: function() {
        return (
            <article className="mhc-product-detail">
                <div className="row">
                    <div className="phone-12 tablet-7 columns mhc-product-img-container">
                        <img src={product.img} className="mhc-product-img" />
                    </div>

                    <div className="phone-12 tablet-5 columns mhc-product-info-container">
                        <div className="inner-outer">
                            <div className="inner-inner">
                                <div className="mhc-product-breadcrumbs">
                                    <span className="mhc-copy">Products</span><span className="mhc-copy">Cream on Top</span><span className="mhc-copy">Plain Yogurt</span>
                                </div>

                                <h3 className="mhc-product-intro">{product.description}</h3>

                                <div className="mhc-section mhc-product-nutrition-facts clearfix">
                                    <div className="row mhc-section-title-container">
                                        <div className="phone-2 columns mhc-section-icon"><img src={products.yogurtPlain.nutritionFacts.icon} /></div>
                                        <div className="phone-8 columns mhc-title">{facts.title}</div>
                                        <div className="phone-2 columns mhc-daily-value-title">{facts.dv}</div>
                                    </div>

                                    <div className={"row " + facts.calories.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.calories.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.calories.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.calories.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.caloriesFromFat.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.caloriesFromFat.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.caloriesFromFat.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.caloriesFromFat.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.totalFat.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.totalFat.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.totalFat.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.totalFat.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.saturatedFat.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.saturatedFat.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.saturatedFat.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.saturatedFat.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.transFat.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.transFat.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.transFat.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.transFat.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.cholesterol.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.cholesterol.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.cholesterol.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.cholesterol.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.sodium.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.sodium.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.sodium.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.sodium.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.carbs.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.carbs.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.carbs.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.carbs.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.dailyFiber.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.dailyFiber.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.dailyFiber.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.dailyFiber.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.sugar.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.sugar.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.sugar.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.sugar.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.protein.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.protein.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.protein.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.protein.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.vitaminA.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.vitaminA.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.vitaminA.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.vitaminA.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.vitaminC.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.vitaminC.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.vitaminC.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.vitaminC.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.calcium.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.calcium.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.calcium.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.calcium.dailyValue)}</div>
                                    </div>

                                    <div className={"row " + facts.iron.slug}>
                                        <div className="phone-8 columns mhc-title">{facts.iron.title}</div>
                                        <div className="phone-2 columns mhc-count">{getValue(facts.iron.count)}</div>
                                        <div className="phone-2 columns mhc-daily-value">{getValue(facts.iron.dailyValue)}</div>
                                    </div>
                                </div>{/* Nutrition Facts */}

                                <div className="mhc-section mhc-product-ingredients clearfix">
                                    <div className="row mhc-section-title-container">
                                        <div className="phone-2 columns mhc-section-icon"><img src={ingredients.icon} /></div>
                                        <div className="phone-10 columns mhc-title">{ingredients.title}</div>
                                    </div>

                                    <div className="row mhc-product-ingredients-list">
                                        <div className="phone-12 columns">
                                            <p>{ingredients.list}</p>
                                        </div>
                                    </div>
                                </div>{/* Ingredients */}

                                <div className="mhc-section mhc-product-certifications clearfix">
                                    <div className="row mhc-section-title-container">
                                        <div className="phone-2 columns mhc-section-icon"><img src={certifications.icon} /></div>
                                        <div className="phone-10 columns mhc-title">{certifications.title}</div>
                                    </div>

                                    <div className="row mhc-product-certification">
                                        <div className="phone-10 columns mhc-title">{certifications.kosher.title}</div>
                                        <div className="phone-2 columns mhc-certification-status">{getValue(certifications.kosher.status)}</div>
                                    </div>

                                    <div className="row mhc-product-certification">
                                        <div className="phone-10 columns mhc-title">{certifications.grassFed.title}</div>
                                        <div className="phone-2 columns mhc-certification-status">{getValue(certifications.grassFed.status)}</div>
                                    </div>

                                    <div className="row mhc-product-certification">
                                        <div className="phone-10 columns mhc-title">{certifications.glutenFree.title}</div>
                                        <div className="phone-2 columns mhc-certification-status">{getValue(certifications.glutenFree.status)}</div>
                                    </div>

                                    <div className="row mhc-product-certification">
                                        <div className="phone-10 columns mhc-title">{certifications.gradeA.title}</div>
                                        <div className="phone-2 columns mhc-certification-status">{getValue(certifications.gradeA.status)}</div>
                                    </div>

                                </div>{/* Certifications */}
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        );
    }
});

module.exports = ProductDetail;