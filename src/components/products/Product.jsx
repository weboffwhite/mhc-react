var React = require('react');

var Product = React.createClass({
    render: function() {
        // var imgIndicator = (this.props.indicator) ? <img src={this.props.indicator} className="mhc-indicator" /> : "";
        // var imgSize      = (this.props.size) ? <h5 className="mhc-size">{this.props.size}</h5> : "";

        return (
            <li className={"mhc-product phone-12 tablet-6 laptop-6 desktop-4 hd-3 columns " + this.props.className}>
                <a className="mhc-product-inner" href={this.props.productName}>
                    <img src={this.props.img} className="mhc-img" />
                    {/* imgIndicator */}
                    {/* imgSize */}
                    <h3 className={"mhc-title mhc-" + this.props.className}>{this.props.title}</h3>
                    <p className="mhc-description">{this.props.description}</p>
                </a>
            </li>            
        );
    }
});

module.exports = Product;