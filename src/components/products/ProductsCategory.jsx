var React = require('react');
var Product = require('./Product.jsx');

var ProductsCategory = React.createClass({
    render: function() {

        var productsList = this.props.productsList;

        var listOfProducts = productsList.map(function(product) {
            return (
                <Product key={product.id} title={product.title} img={product.img} description={product.description} className={product.className} />
            );
        });

        return (
            <div className={"row mhc-products-category " + this.props.className}>
                <h3 className="mhc-title">{this.props.title}</h3>
                <div className="mhc-description" dangerouslySetInnerHTML={{__html: this.props.description}} />
                <ul className="mhc-product-grid row">
                    {listOfProducts}
                </ul>
            </div>
        );
    }
});

module.exports = ProductsCategory;