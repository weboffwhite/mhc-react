var React = require('react');
var Hero = require('./global/Hero.jsx');
var ProductDetail = require('./products/ProductDetail.jsx');

var PageAbout = React.createClass({
    render: function() {
        return (
            <div id="mhcAbout" className="mhc-page-body">
                <Hero callouts={false} 
                      img={"img/mhc-products-bg.jpg"} 
                      pretitle={"Certified 100% Grass Fed"} 
                      title={"Products"} 
                      description={"productsHeroDescription"} 
                      callToAction={"About"}
                      callToActionLink={"#/about"} />
                <h1>Phack yaw</h1>
                <ProductDetail />
            </div>
        );
    }
});

module.exports = PageAbout;