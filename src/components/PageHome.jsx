var React          = require('react');
var Hero           = require('./global/Hero.jsx');
var HomeFeature    = require('./home/HomeFeature.jsx');
var HomeAbout      = require('./home/HomeAbout.jsx');
var HomeNewsletter = require('./home/HomeNewsletter.jsx');
var HomeMoreInfo   = require('./home/HomeMoreInfo.jsx');
var HomeVideoModal = require('./home/HomeVideoModal.jsx');

var counter = 0;
var calloutsList = [
    { "id": counter++, "title": "100% Grass-Fed", "subtitle": "Products", "className": "products", "img": "img/maplehill-grass-fed-products.jpg", "link": "#/products" },
    { "id": counter++, "title": "100% Grass-Fed", "subtitle": "Products", "className": "products", "img": "img/maplehill-grass-fed-about.jpg", "link": "#/about" },
    { "id": counter++, "title": "100% Grass-Fed", "subtitle": "Products", "className": "products", "img": "img/maplehill-grass-fed-story.jpg", "link": "#/story" },
    { "id": counter++, "title": "100% Grass-Fed", "subtitle": "Products", "className": "products", "img": "img/maplehill-grass-fed-mission.jpg", "link": "#/mission" }
];

var homeHeroDescription = "Converting farms to the 100% grass-fed model, and improving the land, the cows, and the farmers lives along the way!";

var PageHome = React.createClass({

    componentDidMount: function() {

        var modalHandler = myHelpers.toggleVideoLightBox.bind(myHelpers);

        modalHandler('is-visible', '.mhc-modal-trigger-video', '.mhc-modal', '#mhcVideo', '.mhc-modal-close');
    },
    render: function() {
        return(
            <div id="mhcHome" className="mhc-page-body">
                <Hero callouts={calloutsList} 
                      img={"img/maplehill-grass-fed-feature-video.jpg"} 
                      pretitle={"Maple Hill Creamery"} 
                      title={"100%<br />Grass-Fed"} 
                      description={homeHeroDescription}
                      callToAction={"Products"}
                      callToActionLink={"#/products"} />
                <HomeFeature />
                <HomeAbout />
                <HomeNewsletter />
                <HomeMoreInfo />
                <HomeVideoModal />
            </div>
        );
    }
});

module.exports = PageHome;
